import { useEffect, useState } from 'react'
import { useParams, Link } from 'react-router-dom'
import { fetchAuthor } from './AuthorAPI'
import Container from '../shared/Container'
import HeaderContainer from '../shared/HeaderContainer'

function Author() {

    const [author, setAuthor] = useState(null)
    const { id } = useParams()

    console.log(id);


    useEffect(() => {
        fetchAuthor(id)
            .then(author => { setAuthor(author) })
            .catch(e => {
                console.log(e.message);
            })
    }, [id])


    return (
        <>
            <HeaderContainer>
                <Container>
                    <Link to="/">Go Back</Link>
                </Container>
            </HeaderContainer>
            { author &&
                <>
                    <Container>
                        <section className="author">
                            <h4>{author.name}</h4>
                            <p>{author.bio.value}</p>
                        </section>
                    </Container>
                </>
            }
        </>
    )
}

export default Author