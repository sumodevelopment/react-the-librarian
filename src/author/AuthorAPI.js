export function fetchAuthor(authorUrl) {
    return fetch(`https://openlibrary.org/authors/${authorUrl}.json`)
        .then(r => r.json())
}