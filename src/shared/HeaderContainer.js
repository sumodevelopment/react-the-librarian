import styles from './HeaderContainer.module.css'

function HeaderContainer(props) {
    return (
        <header className={ styles.HeaderContainer }>
            { props.children }
        </header>
    )
}

export default HeaderContainer