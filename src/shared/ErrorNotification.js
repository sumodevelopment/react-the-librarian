import styles from './ErrorNotification.module.css'

function ErrorNotification({ title = '', message ='' }) {
    return (
        <div className={ styles.ErrorNotification }>  
            <h4>{ title }</h4>
            <p>{ message }</p>
        </div>
    )
}

export default ErrorNotification