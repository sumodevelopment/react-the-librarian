import { useState } from 'react'
import { BOOK_SEARCH_TYPE } from '../book/BookAPI'
import Container from '../shared/Container'
import HeaderContainer from '../shared/HeaderContainer'
import styles from './Search.module.css'

function Search(props) {

    // 9780345453747
    const [searchQuery, setSearchQuery] = useState('')
    const [searchType, setSearchType] = useState(BOOK_SEARCH_TYPE.WorkId)

    const onSearchTypeChanged = e => {
        setSearchType(e.target.value)
    }

    const onSearchQueryChanged = event => {
        setSearchQuery(event.target.value)
    }

    const onSearchClicked = () => {
        props.clicked({
            type: searchType,
            query: searchQuery
        })
    }

    return (
        <HeaderContainer>
            <Container>
                <h2 className={styles.SearchTitle}>The Librarian</h2>
                <div className={styles.SearchControls}>
                    <select className={styles.SearchTypeSelect} onChange={onSearchTypeChanged}>
                        <option value={ BOOK_SEARCH_TYPE.WorkId }>By Works ID</option>
                        <option value={ BOOK_SEARCH_TYPE.Isbn }>By ISBN</option>
                    </select>
                    <input
                        className={styles.SearchInput}
                        type="text"
                        placeholder="Enter an search query"
                        value={searchQuery} onChange={onSearchQueryChanged} />
                    <button className={styles.SearchButton} onClick={onSearchClicked}>Search</button>
                </div>
            </Container>
        </HeaderContainer>
    )
}

export default Search