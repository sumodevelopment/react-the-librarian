import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { fetchAuthor } from "../author/AuthorAPI"

function BookAuthorLink({ authorKey }) {

    const [author, setAuthor] = useState(null)

    useEffect(() => {
        fetchAuthor(authorKey)
            .then(author => {
                setAuthor(author)
            })
            .catch(error => {
                console.log('Author Link Error', error);
            })
    }, [ authorKey ])

    return (
        <>
            {  author === null && <p>Loading author...</p>}
            { author &&
                <Link to={`/author/${authorKey}`}>{ author.name }</Link>
            }
        </>
    )
}

export default BookAuthorLink