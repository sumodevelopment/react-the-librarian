import { useReducer } from 'react'
import Search from '../search/Search'
import Container from '../shared/Container'

import { searchBook } from './BookAPI'
import ErrorNotification from '../shared/ErrorNotification';
import LoadingSpinner from '../shared/LoadingSpinner';
import BookHeader from './BookHeader'
import BookAuthors from './BookAuthors';

const ACITON_SET_BOOK = 'book:set-book'
const ACITON_SET_LOADING = 'book:set-loading'
const ACITON_SET_ERROR = 'book:set-error'

function bookReducer(state, action) {
    switch (action.type) {
        case ACITON_SET_BOOK:
            return {
                loading: false,
                error: null,
                book: action.payload
            }
        case ACITON_SET_LOADING:
            return {
                error: null,
                book: null,
                loading: action.payload
            }
        case ACITON_SET_ERROR:
            return {
                book: null,
                loading: false,
                error: action.payload
            }
        default:
            return state
    }
}

const initialState = {
    book: null,
    loading: false,
    error: null
}


function Book() {

    const [state, dispatch] = useReducer(bookReducer, initialState)

    // HANDLERS
    const handleSearchClick = async ({ type, query }) => {
        try {
            dispatch({ type: ACITON_SET_LOADING, payload: true })
            const book = await searchBook(type, query)
            dispatch({ type: ACITON_SET_BOOK, payload: book })
        } catch (e) {
            dispatch({ type: ACITON_SET_ERROR, payload: e.message })
        }
    }

    return (
        <>
            <Search clicked={handleSearchClick} />

            <Container>
                {state.loading && <LoadingSpinner />}

                {state.error &&
                    <ErrorNotification title="Error" message={state.error} />
                }
                
                {state.book &&
                    <>
                        <BookHeader book={state.book} />
                        <BookAuthors authors={state.book.authors} />
                    </>
                }
            </Container>
        </>
    )

}

export default Book