export function searchBook(type, query) {
    return fetch(`https://openlibrary.org/${type}/${query}.json`)
        .then(r => r.json())
        .then(checkNotFound)
        .then(setBookCover)
        .then(setBookAuthorUrl)
}

export const BOOK_SEARCH_TYPE = {
    WorkId: 'works',
    Isbn: 'isbn'
}

function checkNotFound(response) {
    if (response.error) {
        throw Error(`The book matching "${response.key.split('/').pop()}" could not be found`);
    }

    return response;
}

function setBookCover(book) {

    if (!book.covers) { return book }

    return {
        ...book,
        coverImage: `http://covers.openlibrary.org/b/ID/${book.covers[0]}-M.jpg`
    }
}

function setBookAuthorUrl(book) {
    // Inconsistent API responses 
    // Set a empty array for Authors in case the prop does not exits. :( 
    if (!book.authors) {
        return {
            ...book,
            authors: []
        }
    }

    const getAuthorId = path => path.split('/').pop()

    return {
        ...book,
        authors: book.authors.map(author => {
            // Inconsistent responses in API.
            if (author.author) {
                return {
                    key: getAuthorId(author.author.key)
                }
            }
            return {
                key: getAuthorId(author.key)
            }
        })
    }
}