import styles from './Book.module.css'

function BookHeader({ book }) {
    return (
        <header className={styles.BookWrapper}>
            <div className={styles.BookCover}>
                <img src={book.coverImage} alt={book.title} />
            </div>

            <article className={styles.BookMeta}>
                <h2>{book.title}</h2>
                <p className={styles.BookPublishedDate}>
                    Published: {book.publish_date}
                </p>
                <p className={styles.BookDescription}>{book.description.value}</p>
            </article>
        </header>
    )
}

export default BookHeader