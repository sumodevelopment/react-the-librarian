import styles from './Book.module.css'
import BookAuthorLink from "./BookAuthorLink";

function BookAuthors({ authors }) {
    return (
        <section className={styles.BookAuthorLinks}>
            <h4>Authors</h4>
            {authors.map(author => (
                <BookAuthorLink key={author.key} authorKey={author.key} />
            ))}
        </section>
    )
}

export default BookAuthors