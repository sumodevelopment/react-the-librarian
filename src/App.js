import './App.css'
import React from 'react'
import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom'

import Book from './book/Book'
import Author from './author/Author'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route exact path="/" component={ Book } />
          <Route path="/author/:id" component={Author} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}

export default App
